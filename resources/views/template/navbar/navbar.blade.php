
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  
  @yield('navbar-left-links', View::make('template.navbar.left-links'))
  @yield('navbar-search-form', View::make('template.navbar.search-form'))
  @yield('navbar-right-links', View::make('template.navbar.right-links'))


</nav>
<!-- /.navbar -->
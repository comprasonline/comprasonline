<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content-header', View::make('template.content.header'))

    @yield('content-body', View::make('template.content.body'))

    
  </div>
  <!-- /.content-wrapper -->
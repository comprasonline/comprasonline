<!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="./index2.html" class="nav-link @yield('sidebar-menu-mi-cuenta-active')">
                <i class="far fa-circle nav-icon"></i>
                <p>Mi cuenta</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="./index2.html" class="nav-link @yield('sidebar-menu-mi-saldo-active')">
                <i class="far fa-circle nav-icon"></i>
                <p>Mi saldo</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="./index2.html" class="nav-link @yield('sidebar-menu-mi-historial-active')">
                <i class="far fa-circle nav-icon"></i>
                <p>Mi historial</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="./index2.html" class="nav-link @yield('sidebar-menu-rifas-active')">
                <i class="far fa-circle nav-icon"></i>
                <p>Rifas</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="./index2.html" class="nav-link @yield('sidebar-menu-remates-active')">
                <i class="far fa-circle nav-icon"></i>
                <p>Remates</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="./index2.html" class="nav-link @yield('sidebar-menu-tendencias-active')">
                <i class="far fa-circle nav-icon"></i>
                <p>Tendencias</p>
            </a>
        </li>
      </ul>
    </nav>
<!-- /.sidebar-menu -->

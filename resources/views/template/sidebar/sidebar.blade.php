<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  @yield('sidebar-brand-logo', View::make('template.sidebar.brand-logo'))

  <!-- Sidebar -->
  <div class="sidebar">
    @yield('sidebar-user-data', View::make('template.sidebar.user-data'))
    @yield('sidebar-menu', View::make('template.sidebar.menu'))
  </div>
  <!-- /.sidebar -->
</aside>
